#!/bin/bash

MAX_TRIES=3

currentTries=`cat /etc/checkInternetConnection/currentTries`


function checkInternetConnection {
  wget -q -T 10 --spider http://google.com

  if [ $? -eq 0 ]; then
      echo "Online"
  else
      echo "Offline"
  fi 
}

function restartVpn {
   service openvpn restart
}

echo `date`

if [ "$currentTries" -ge "$MAX_TRIES" ]; then
   echo "Number of trials exceeded: $currentTries/$MAX_TRIES"
   exit 1;
fi

echo "Trial $currentTries out of $MAX_TRIES"

internetConnectionState=$(checkInternetConnection)
echo "Internet connection state is $internetConnectionState"

if [ "$internetConnectionState" = "Offline" ]; then
   echo "Internet connection problem - restarting VPN"
   restartVpn
   internetConnectionStateAfterVpnRestart=`checkInternetConnection`
   echo "Internet connection is $internetConnectionStateAfterVpnRestart after VPN restart"
   echo $((currentTries+1)) > /etc/checkInternetConnection/currentTries 
else
   echo 0 > /etc/checkInternetConnection/currentTries
   echo "Connection is Online - no action required - reseting trials counter"
fi  
